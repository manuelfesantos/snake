import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.io.*;

public class Game implements Runnable {

    public Grid grid;
    public Snake snake;
    public Apple apple;
    public boolean gameOver;
    public InfoPanel infoPanel;

    public Text endGame;
    public Text restart;
    public Text restartOptions;
    public int highScore;
    private int counter;
    private int superCounter;
    private int superTimer;
    public SuperApple superApple;
    public boolean existsSuper;

    public Game() {
        grid = new Grid();
        snake = new Snake(this);
        apple = new Apple(snake, Color.RED);
        endGame = new Text(Field.SCREEN_WIDTH/2f, Field.SCREEN_HEIGHT/2f, "YOU LOSE");
        endGame.grow(70, 50);
        endGame.translate(70, 40);
        endGame.translate(-endGame.getWidth()/2f, -endGame.getHeight()/2f);
        restart = new Text(Field.SCREEN_WIDTH/2f, Field.SCREEN_HEIGHT/2f, "Restart?");
        restart.grow(40, 10);
        restart.translate(-10, 50);
        restartOptions = new Text(Field.SCREEN_WIDTH/2f, Field.SCREEN_HEIGHT/2f, "Y/N");
        restartOptions.grow(20, 10);
        restartOptions.translate(-10, 100);
        gameOver = false;
        infoPanel = new InfoPanel(this);
        loadHighScore();
        counter = 0;
        superCounter = 0;
        existsSuper = false;
        superTimer = 0;
    }


    @Override
    public void run() {
        while(!gameOver) {
            try {

                if(!gameOver) {
                    update();

                } else {
                    saveHighScore();
                    endGame.draw();
                    restart.draw();
                    restartOptions.draw();

                    if(snake.keyHandler.yes) {
                        restart();
                    }
                    if(snake.keyHandler.no) {
                        System.exit(0);
                    }

                }

                Thread.sleep(80);

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }




/*
        while(true) {
            if(counter == 0) {
                counter = 20;
            }
            if(counter >10) {
                endGame.grow(5, 5);
            } else {
                endGame.grow(-5, -5);
            }
            for(int i = 0; i < snake.snake.size(); i++) {
                if(counter > 10) {
                    snake.snake.get(i).setColor(Color.RED);
                } else {
                    snake.snake.get(i).setColor(Color.GREEN);
                }
            }
            counter--;
            try {
                Thread.sleep(41);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }*/



    }

    public void update() {

        if(snake.snake.get(0).getX() == apple.apple.getX() && snake.snake.get(0).getY() == apple.apple.getY()) {
            apple.apple.delete();
            apple = new Apple(snake, Color.RED);
            snake.snake.add(new Rectangle(snake.snake.get(1).getX(), snake.snake.get(1).getY(), Field.TILE_SIZE, Field.TILE_SIZE));
            snake.snake.get(snake.snake.size()-1).setColor(Color.GREEN);
            snake.snake.get(snake.snake.size()-1).fill();
            infoPanel.pointCounter++;
        }
        if(superApple != null) {
            if (snake.snake.get(0).getX() == superApple.apple.getX() && snake.snake.get(0).getY() == superApple.apple.getY()) {
                superApple.apple.delete();
                snake.snake.add(new Rectangle(snake.snake.get(1).getX(), snake.snake.get(1).getY(), Field.TILE_SIZE, Field.TILE_SIZE));
                snake.snake.get(snake.snake.size()-1).setColor(Color.GREEN);
                snake.snake.get(snake.snake.size()-1).fill();
                infoPanel.pointCounter += 5;
                existsSuper = false;
                superApple = null;
            }
        }
        if(infoPanel.pointCounter > highScore) {
            highScore = infoPanel.pointCounter;
        }
        if(!existsSuper) {
            if (superCounter == 0) {
                superCounter = (int) (Math.random() * 50 + 50);
            }
            superCounter--;
            if(superCounter == 0) {
                existsSuper = true;
            }
        }
        if(existsSuper) {
            if(superTimer == 0) {
                superApple = new SuperApple(snake);
                superTimer = 30;
            }
            superTimer--;
            if(superTimer <= 0 && superApple != null) {
                superApple.apple.delete();
                superApple = null;
                existsSuper = false;
            }
        }

        snake.update();
        infoPanel.update();
    }




    public void loadHighScore() {
        try {
            FileReader fileReader = new FileReader("scores/scoreSheet.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            if((line = bufferedReader.readLine()) != null) {
                highScore = Integer.parseInt(line);
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveHighScore() {
        try {
            FileWriter fileWriter = new FileWriter("scores/scoreSheet.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("" + highScore);

            bufferedWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void restart() {



    }
}
