import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;
import java.util.LinkedList;

public class Snake {

    public ArrayList<Rectangle> snake;
    public KeyHandler keyHandler;
    public Game game;
    public Direction direction;

    public Snake(Game game) {
        snake = new ArrayList<>();
        this.game = game;
        for(int i = 0; i < 5; i++) {
            snake.add(new Rectangle(Field.PADDING + Field.MAX_COLS*Field.TILE_SIZE/2-Field.TILE_SIZE*i, Field.PADDING + Field.MAX_ROWS*Field.TILE_SIZE/2, Field.TILE_SIZE, Field.TILE_SIZE));
            snake.get(i).setColor(Color.GREEN);
            snake.get(i).fill();
        }
        direction = Direction.RIGHT;
        snake.get(0).setColor(Color.RED);

        keyHandler = new KeyHandler(this);
    }

    public void update() {
        if(keyHandler.up) {
            int x = snake.get(0).getX();
            int y = snake.get(0).getY();
            int x2 = 0;
            int y2 = 0;
            for (int i = 0; i < snake.size(); i++) {
                if(i == 0) {
                    snake.get(i).translate(0, snake.get(i).getY() <= Field.PADDING ? Field.SCREEN_HEIGHT-10-Field.TILE_SIZE : -Field.TILE_SIZE);
                    continue;
                }
                if(i % 2 != 0) {
                    x2 = snake.get(i).getX();
                    y2 = snake.get(i).getY();
                    snake.get(i).translate(x - snake.get(i).getX(), y - snake.get(i).getY());
                }
                if(i % 2 == 0) {
                    x = snake.get(i).getX();
                    y = snake.get(i).getY();
                    snake.get(i).translate(x2 - snake.get(i).getX(), y2 - snake.get(i).getY());
                }



            }
            direction = Direction.UP;
        } else if(keyHandler.down) {
                int x = snake.get(0).getX();
                int y = snake.get(0).getY();
                int x2 = 0;
                int y2 = 0;
                for (int i = 0; i < snake.size(); i++) {
                    if(i == 0) {
                        snake.get(i).translate(0, snake.get(i).getY() + snake.get(i).getHeight() >= Field.SCREEN_HEIGHT ? -Field.SCREEN_HEIGHT+10+Field.TILE_SIZE : Field.TILE_SIZE);
                        continue;
                    }
                    if(i % 2 != 0) {
                        x2 = snake.get(i).getX();
                        y2 = snake.get(i).getY();
                        snake.get(i).translate(x - snake.get(i).getX(), y - snake.get(i).getY());
                    }
                    if(i % 2 == 0) {
                        x = snake.get(i).getX();
                        y = snake.get(i).getY();
                        snake.get(i).translate(x2 - snake.get(i).getX(), y2 - snake.get(i).getY());
                    }
                direction = Direction.DOWN;
                }
        }else if(keyHandler.left) {
            int x = snake.get(0).getX();
            int y = snake.get(0).getY();
            int x2 = 0;
            int y2 = 0;
            for (int i = 0; i < snake.size(); i++) {
                if(i == 0) {
                    snake.get(i).translate(snake.get(i).getX() <= Field.PADDING ? Field.SCREEN_WIDTH-10-Field.TILE_SIZE: -Field.TILE_SIZE, 0);
                    continue;
                }
                if(i % 2 != 0) {
                    x2 = snake.get(i).getX();
                    y2 = snake.get(i).getY();
                    snake.get(i).translate(x - snake.get(i).getX(), y - snake.get(i).getY());
                }
                if(i % 2 == 0) {
                    x = snake.get(i).getX();
                    y = snake.get(i).getY();
                    snake.get(i).translate(x2 - snake.get(i).getX(), y2 - snake.get(i).getY());
                }
            }
            direction = Direction.LEFT;
        }else if(keyHandler.right) {
            int x = snake.get(0).getX();
            int y = snake.get(0).getY();
            int x2 = 0;
            int y2 = 0;
            for (int i = 0; i < snake.size(); i++) {
                if(i == 0) {
                    snake.get(i).translate(snake.get(i).getX() + snake.get(i).getWidth() >= Field.SCREEN_WIDTH ? -Field.SCREEN_WIDTH+10+Field.TILE_SIZE : Field.TILE_SIZE, 0);
                    continue;
                }
                if(i % 2 != 0) {
                    x2 = snake.get(i).getX();
                    y2 = snake.get(i).getY();
                    snake.get(i).translate(x - snake.get(i).getX(), y - snake.get(i).getY());
                }
                if(i % 2 == 0) {
                    x = snake.get(i).getX();
                    y = snake.get(i).getY();
                    snake.get(i).translate(x2 - snake.get(i).getX(), y2 - snake.get(i).getY());
                }
            }
            direction = Direction.RIGHT;
        }

        for(int i = 0; i < snake.size(); i++) {
            if(i == 0) {
                snake.get(0).fill();
                continue;
            }
            if(snake.get(0).getX() == snake.get(i).getX() && snake.get(0).getY() == snake.get(i).getY()) {
                game.gameOver = true;
            }
            snake.get(i).fill();
        }




    }

}
