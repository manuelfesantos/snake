import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Ellipse;

public class Apple {

    public Ellipse apple;
    public Snake snake;

    public Apple(Snake snake, Color color) {
        this.snake = snake;
        apple = new Ellipse(10 + (int)(Math.random()*Field.MAX_COLS)*Field.TILE_SIZE, 10 + (int)(Math.random()*Field.MAX_ROWS) * Field.TILE_SIZE, Field.TILE_SIZE, Field.TILE_SIZE);
        boolean resetApple = true;
        while(resetApple) {
            resetApple = false;
            for (int i = 0; i < snake.snake.size(); i++) {
                if (apple.getX() == snake.snake.get(i).getX() && apple.getY() == snake.snake.get(i).getY()) {
                    resetApple = true;
                    apple = new Ellipse(10 + (int)(Math.random()*Field.MAX_COLS)*Field.TILE_SIZE, 10 + (int)(Math.random()*Field.MAX_ROWS) * Field.TILE_SIZE, Field.TILE_SIZE, Field.TILE_SIZE);
                }
            }
        }
        apple.setColor(color);
        apple.fill();
    }
}
