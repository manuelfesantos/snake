import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Grid {

    public Rectangle[][] grid;
    public Rectangle gridPadding;

    public Grid() {
        grid = new Rectangle[Field.MAX_ROWS][Field.MAX_COLS];
        gridPadding = new Rectangle(Field.PADDING, Field.PADDING, Field.SCREEN_WIDTH-Field.PADDING, Field.SCREEN_HEIGHT-Field.PADDING);
        makeGrid();
    }

    public void makeGrid() {
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[i].length; j++) {
                grid[i][j] = new Rectangle(Field.PADDING + j * Field.TILE_SIZE, Field.PADDING + i * Field.TILE_SIZE, Field.TILE_SIZE, Field.TILE_SIZE);
                grid[i][j].setColor(Color.WHITE);
                grid[i][j].draw();
            }
        }
        gridPadding.draw();
    }

}
