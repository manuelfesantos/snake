public class Field {

    public static final int TILE_SIZE = 20;
    public static final int MAX_COLS = 20;
    public static final int MAX_ROWS = 20;
    public static final int PADDING = 10;
    public static final int SCREEN_WIDTH = MAX_COLS * TILE_SIZE + PADDING;
    public static final int SCREEN_HEIGHT = MAX_ROWS * TILE_SIZE + PADDING;
    public static final int INFO_PANEL_X = SCREEN_WIDTH;
    public static final int INFO_PANEL_Y = PADDING;
    public static final int INFO_PANEL_WIDTH = TILE_SIZE*8;
    public static final int INFO_PANEL_HEIGHT = SCREEN_HEIGHT;

}
