import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyHandler implements KeyboardHandler {

    private KeyboardEvent[] keyboardEvents;
    private Keyboard keyboard;
    public Snake snake;

    public boolean up, down, left, right, yes, no;

    public KeyHandler(Snake snake) {
        this.snake = snake;
        keyboard = new Keyboard(this);
        keyboardEvents = new KeyboardEvent[7];
        createEvents();
    }

    public void createEvents() {
        for(int i = 0; i < keyboardEvents.length; i++) {
            keyboardEvents[i] = new KeyboardEvent();
        }
        keyboardEvents[0].setKey(KeyboardEvent.KEY_W); //UP
        keyboardEvents[1].setKey(KeyboardEvent.KEY_S);
        keyboardEvents[2].setKey(KeyboardEvent.KEY_A);
        keyboardEvents[3].setKey(KeyboardEvent.KEY_D);
        keyboardEvents[4].setKey(27);
        keyboardEvents[5].setKey(KeyboardEvent.KEY_Y);
        keyboardEvents[6].setKey(KeyboardEvent.KEY_N);

        for(int i = 0; i < keyboardEvents.length; i++) {
            keyboardEvents[i].setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(keyboardEvents[i]);
            KeyboardEvent ke = new KeyboardEvent();
            ke.setKey(keyboardEvents[i].getKey());
            ke.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
            keyboard.addEventListener(ke);
        }
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if(keyboardEvent.getKey() == keyboardEvents[0].getKey() && snake.direction != Direction.DOWN) {
            resetKeys();
            up = true;
        }
        if(keyboardEvent.getKey() == keyboardEvents[1].getKey() && snake.direction != Direction.UP) {
            resetKeys();
            down = true;
        }
        if(keyboardEvent.getKey() == keyboardEvents[2].getKey() && snake.direction != Direction.RIGHT) {
            resetKeys();
            left = true;
        }
        if(keyboardEvent.getKey() == keyboardEvents[3].getKey() && snake.direction != Direction.LEFT) {
            resetKeys();
            right = true;
        }
        if(keyboardEvent.getKey() == keyboardEvents[4].getKey()) {
            System.exit(0);
        }
        if(keyboardEvent.getKey() == keyboardEvents[5].getKey() && snake.game.gameOver) {
            yes = true;
        }
        if(keyboardEvent.getKey() == keyboardEvents[6].getKey() && snake.game.gameOver) {
            no = true;
        }
    }

    public void resetKeys() {
        up = false;
        down = false;
        left = false;
        right = false;
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        /*
        if(keyboardEvent.getKey() == keyboardEvents[0].getKey()) {
            up = false;
        }
        if(keyboardEvent.getKey() == keyboardEvents[1].getKey()) {
            down = false;
        }
        if(keyboardEvent.getKey() == keyboardEvents[2].getKey()) {
            left = false;
        }
        if(keyboardEvent.getKey() == keyboardEvents[3].getKey()) {
            right = false;
        }

         */
        if(keyboardEvent.getKey() == keyboardEvents[5].getKey()) {
            yes = false;
        }
        if(keyboardEvent.getKey() == keyboardEvents[6].getKey() && snake.game.gameOver) {
            no = false;
        }
    }
}
