import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;

public class InfoPanel {

    public Rectangle panel;
    public int pointCounter;
    public Text counterText;
    public Text maxScoreText;
    public Game game;



    public InfoPanel(Game game) {
        this.game = game;
        panel = new Rectangle(Field.INFO_PANEL_X, Field.INFO_PANEL_Y, Field.INFO_PANEL_WIDTH, Field.TILE_SIZE*4-Field.PADDING);
        pointCounter = 0;
        counterText = new Text(Field.INFO_PANEL_X + Field.TILE_SIZE, Field.INFO_PANEL_Y + Field.TILE_SIZE, "Score: "+pointCounter);
        maxScoreText = new Text(Field.INFO_PANEL_X + Field.TILE_SIZE, Field.INFO_PANEL_Y + Field.TILE_SIZE*2, "High Score: "+game.highScore);
        panel.draw();
        counterText.draw();
        maxScoreText.draw();
    }

    public void update() {
        counterText.setText("Score: " + pointCounter);
        maxScoreText.setText("High Score: " + game.highScore);
    }

}
